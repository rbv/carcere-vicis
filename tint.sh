#!/bin/bash

convert "$1" -crop 320x200 +repage \
          -channel R -evaluate multiply 0.60 \
          -channel B -evaluate multiply 0.80 \
          -channel G -evaluate multiply 0.60 \
 -remap  "Neo's Better Palette.bmp"  "$2"
#          -dither FloydSteinberg

