################################################################ ITEM DEFINITIONS

#            TYPE      SIZE   HEAPNAME
set sizeof   ItemDef  ,   25  ,  LARGEHEAP   # Don't forget to inc "heap LARGEHEAP sizeof" if smaller
defineconstant(25, sizeof:ItemDef)   # Don't forget to updated static itemdef layout!

# ItemDef inherits from Noun:
# defineconstant(0, idx:name!)
# defineconstant(1, idx:pronoun!)
# defineconstant(2, idx:unique)  # true, false, or PROPER_NOUN
# defineconstant(3, idx:always_plural)  # eg Pills
defineconstant(4, idx:spritetype)   # duplicated
defineconstant(5, idx:spriteset)   # duplicated
#defineconstant(6, idx:flags)      # duplicated
#defineconstant(7, idx:damage_min)  # only for weapons, duplicated
#defineconstant(8, idx:damage_max)  # only for weapons, duplicated
defineconstant(9, idx:description!)  # duplicated
#defineconstant(9, idx:verb!)       # only for weapons (unimplemented)
#defineconstant(10, idx:damage_verb!)     # only for weapons. Verb used when damaging equip (unimplemented)

defineconstant(11, idx:defense)    # only for weapons and armour
defineconstant(12, idx:coverage)   # only for weapons and armour
defineconstant(13, idx:speed_bonus)  # negative for penalty
defineconstant(14, idx:destructive)  # 0-100 ability to damage equipment
defineconstant(15, idx:itemtype)   # one of the ITEM_* constants
defineconstant(16, idx:basevalue)  # gold
defineconstant(17, idx:rarity)     # proportional to chance of generation
defineconstant(18, idx:generated)  # counts number of times generated.
defineconstant(19, idx:possess_tag) # tag to turn on when in possession
defineconstant(20, idx:sprite_width) # in pixels. Only used for goal items
defineconstant(21, idx:maxcharges)  # Max value to generate for 'charges', or 0 if doesn't have charges
#defineconstant(24, idx:difficulty) # typical dungeon difficulty at which it is found (duplicated)

# For weapons, defense determines strength for parrying.
# If the defense is 0, it's not used for parrying.


################ flags bits
defineconstant(1, IDENTIFIED)  #unimplemented
defineconstant(2, IS_WEAPON)
defineconstant(4, IS_ARMOUR)
defineconstant(8, STACKABLE)
defineconstant(16, NON_REUSABLE) # for scrolls
defineconstant(32, GENERATE_SINGLE)
defineconstant(64, CHANNEL_MAGIC)  # useful for mages
defineconstant(128, MAGIC_DAMAGE)  # Different damage calculation

################ item types
defineconstant(-13000, ITEM_OTHER)
defineconstant(-13001, ITEM_POTION)
defineconstant(-13002, ITEM_SCROLL)  # Not including quest items
defineconstant(-13003, ITEM_CLOTH)
defineconstant(-13004, ITEM_BLADE)
defineconstant(-13005, ITEM_OTHERWEAPON)
defineconstant(-13006, ITEM_OTHERARMOUR)
defineconstant(-13007, ITEM_PLATEARMOUR)


# Set up default stats of an Item
script, default_itemdef, this, name, begin
    # Free anything already in the slot, to allow reloading
    freestring(this.name!)
    freestring(this.pronoun!)
    # freestring(this.verb!)
    # freestring(this.damage_verb!)

    this.name! := savestring(name)
    this.pronoun! := savestring($"it")
    #this.verb! := 0  # defaults to savestring($"hit")
    #this.damage_verb! := 0 # defaults to savestring($"shatter")
    this.spritetype := spritetype:weapon
    this.spriteset := 0
    this.unique := false
    #this.always_plural := false
    this.basevalue := 0  # Not sellable
    this.bonus := 0
    this.flags := 0
    this.rarity := 0   # Not randomly generated
    this.difficulty := 0
    this.generated := 0
    this.possess_tag := 0
    this.sprite_width := 11
    this.maxcharges := 0
    this.itemtype := ITEM_OTHER
    return(this)
end

script, weapon_itemdef, this, name, begin
    default_itemdef(this, name)
    this.flags += IS_WEAPON
    this.itemtype := ITEM_OTHERWEAPON
    this.coverage := 20  # base parry %
    this.defense := 4
    return(this)
end

script, armour_itemdef, this, name, begin
    default_itemdef(this, name)
    this.flags += IS_ARMOUR
    this.itemtype := ITEM_CLOTH
    this.coverage := 40
    this.defense := 2
    return(this)
end

## 3000 -- 4500 are item defs, size 20

defineconstant(3000, FIRST_ITEM)
defineconstant(3000, itemdef:talisman)
defineconstant(3025, itemdef:nexus_scroll)
defineconstant(3050, itemdef:lighthouse_scroll)
defineconstant(3075, itemdef:basement_key)
defineconstant(3100, itemdef:hppot)
defineconstant(3125, itemdef:regenpot)
defineconstant(3150, itemdef:energypot)
defineconstant(3175, itemdef:berserkpot)
defineconstant(3200, itemdef:engagementring)
defineconstant(3225, itemdef:tiara)
defineconstant(3250, itemdef:tunic)
defineconstant(3275, itemdef:robe)
defineconstant(3300, itemdef:platemail)
defineconstant(3325, itemdef:leather)
defineconstant(3350, itemdef:chainmail)
defineconstant(3375, itemdef:stonemail)
defineconstant(3400, itemdef:leathersteel)
defineconstant(3425, itemdef:sword)
defineconstant(3450, itemdef:dagger)
defineconstant(3475, itemdef:staff)
defineconstant(3500, itemdef:scimitar)
defineconstant(3525, itemdef:katana)
defineconstant(3550, itemdef:scythe)
defineconstant(3575, itemdef:wand)
defineconstant(3600, itemdef:machete)
defineconstant(3625, itemdef:handaxe)
defineconstant(3650, itemdef:battleaxe)
defineconstant(3675, itemdef:rapier)
defineconstant(3700, itemdef:claymore)
defineconstant(3725, itemdef:greatsword)
defineconstant(3750, itemdef:metalclaws)
defineconstant(3775, FIRST_GENERATEABLE_ARTIFACT)
defineconstant(3775, itemdef:invidia)
defineconstant(3800, itemdef:ira)
defineconstant(3825, itemdef:superbia)
defineconstant(3850, itemdef:acedia)
defineconstant(3875, itemdef:avaritia)
defineconstant(3900, itemdef:gula)
defineconstant(3925, itemdef:luxuria)
defineconstant(3925, LAST_GENERATEABLE_ARTIFACT)
defineconstant(3950, itemdef:inquies)
defineconstant(3975, itemdef:lumia)

defineconstant(4000, itemdef:immortality_orb)
defineconstant(4025, FIRST_TRANSMUTE_RESULT)
defineconstant(4025, itemdef:royal_talisman)
defineconstant(4050, itemdef:speed_scroll)
defineconstant(4075, itemdef:teleport_scroll)
defineconstant(4100, itemdef:fireball_scroll)
defineconstant(4125, itemdef:reveal_scroll)
defineconstant(4125, LAST_TRANSMUTE_RESULT)
defineconstant(4150, itemdef:destruction_scroll)
defineconstant(4175, itemdef:enchant_scroll)
defineconstant(4200, itemdef:hppot2)
defineconstant(4225, itemdef:lightning_staff)
defineconstant(4225, LAST_ITEM)


script, define_item_types, begin
    var(this)

    ########################################### QUEST ITEMS

    this := default_itemdef(itemdef:talisman, $"Talisman of Egression")
    this.description! := savestring($"An artifact with which you have learnt to tunnel through to different plains of relaty, by expending the collected lifeforce of slain horrors.")
    this.unique := true
    this.spriteset := 45
    this.spritetype := spritetype:walkabout

    #itemdef:nexus_scroll1, itemdef:nexus_scroll2, itemdef:nexus_scroll3, itemdef:nexus_scroll4)
    this := default_itemdef(itemdef:nexus_scroll, $"Nexus scroll fragment")
    # The description changes when you have 3+ pieces, see item_get_description
    this.description! := savestring($"Part of a spell of teleportation to The Nexus, but is of no use without the other parts.")
    this.spriteset := 26
    this.spritetype := spritetype:walkabout
    this.sprite_width := 12
    this.unique := false
    this.flags += STACKABLE
    # Kludge: this only affects the dungeon info menu item appearance
    # FIXME: not working anyway
    this.possess_tag := tag:Unlock Nexus

    this := default_itemdef(itemdef:lighthouse_scroll, $"Enchanted Logbook")
    this.description! := savestring($"A mysterious book written in strange, almost painted, lettering. You think it's a captain's logbook.")
    this.spriteset := 27
    this.spritetype := spritetype:walkabout
    this.unique := true
    this.possess_tag := tag:Painting Scroll

    this := default_itemdef(itemdef:basement_key, $"Basement Key")
    this.description! := savestring($"A heavy iron key which unlocks the Basement of Nightmares.")
    this.spriteset := 34
    this.unique := true
    this.possess_tag := tag:Basement Key

    this := default_itemdef(itemdef:immortality_orb, $"Orb of Immortality")
    this.description! := savestring($"The object of your quest. As you look into it you see visions of endless wealth and power!")
    this.spriteset := 26
    this.sprite_width := 10
    this.unique := true

    ########################################### POTIONS

    this := default_itemdef(itemdef:hppot, $"potion of healing")
    this.description! := savestring($"Magically immediately restores the user to better health.")
    this.spriteset := 0
    this.flags += STACKABLE
    this.itemtype := ITEM_POTION
    this.rarity := 150
    this.difficulty := 200

    this := default_itemdef(itemdef:hppot2, $"greater potion of healing")
    this.description! := savestring($"Magically immediately restores the user to much better health.")
    this.spriteset := 0
    this.flags += STACKABLE
    this.itemtype := ITEM_POTION
    this.rarity := 90
    this.difficulty := 310

    this := default_itemdef(itemdef:regenpot, $"potion of regeneration")
    this.description! := savestring($"Magically enormously accelerates the healing processes.")
    this.spriteset := 0
    this.flags += STACKABLE
    this.itemtype := ITEM_POTION
    this.rarity := 150
    this.difficulty := 200

    this := default_itemdef(itemdef:energypot, $"potion of energy")
    this.description! := savestring($"Elemental energy concentrated into a form that can be drunk, albeit at serious risk to the unprepared user.")
    this.spriteset := 0
    this.flags += STACKABLE
    this.itemtype := ITEM_POTION
    this.rarity := 50
    this.difficulty := 200

    this := default_itemdef(itemdef:berserkpot, $"potion of berserk")
    this.description! := savestring($"Temporarily gives its user madness and the strength of an ox.")
    this.spriteset := 0
    this.flags += STACKABLE
    this.itemtype := ITEM_POTION
    this.rarity := 50
    this.difficulty := 290

    ########################################### SCROLLS

    this := default_itemdef(itemdef:speed_scroll, $"spell of speeding")
    this.description! := savestring($"Temporarily speeds up its user.")
    this.spriteset := 26
    this.spritetype := spritetype:walkabout
    this.flags += STACKABLE + NON_REUSABLE
    this.itemtype := ITEM_SCROLL
    this.rarity := 35
    this.difficulty := 200

    this := default_itemdef(itemdef:teleport_scroll, $"teleport spell")
    this.description! := savestring($"A scroll containing a spell that teleports its user away.")
    this.spriteset := 26
    this.spritetype := spritetype:walkabout
    this.flags += STACKABLE
    this.itemtype := ITEM_SCROLL
    this.rarity := 40
    this.difficulty := 230

    this := default_itemdef(itemdef:fireball_scroll, $"scroll of fireballs")
    this.description! := savestring($"A spell that summons flames to burn all opponents within a few step's distance.")
    this.spriteset := 26
    this.spritetype := spritetype:walkabout
    this.flags += STACKABLE
    this.itemtype := ITEM_SCROLL
    this.rarity := 28
    this.difficulty := 270

    # unimplmeneted
    this := default_itemdef(itemdef:destruction_scroll, $"spell of destruction")
    this.description! := savestring($"A dangerous spell in unexperienced hands! Destroys things.")
    this.spriteset := 26
    this.spritetype := spritetype:walkabout
    this.flags += STACKABLE + NON_REUSABLE
    this.itemtype := ITEM_SCROLL
    this.rarity := 0
    this.difficulty := 350

    this := default_itemdef(itemdef:reveal_scroll, $"scroll of mapping")
    this.description! := savestring($"A map of the surrounding area will reveal itself on this parchment upon activation.")
    this.spriteset := 26
    this.spritetype := spritetype:walkabout
    this.flags += STACKABLE
    this.itemtype := ITEM_SCROLL
    this.rarity := 50
    this.difficulty := 150

    this := default_itemdef(itemdef:enchant_scroll, $"scroll of enchanting")
    this.description! := savestring($"Reading out this spell while holding a weapon will place an enchantment upon it, improving it.")
    this.spriteset := 26
    this.spritetype := spritetype:walkabout
    this.flags += STACKABLE + NON_REUSABLE
    this.itemtype := ITEM_SCROLL
    this.rarity := 25
    this.difficulty := 290

    ########################################### MISC

    this := default_itemdef(itemdef:engagementring, $"engagement ring")
    this.description! := savestring($"A golden ring; must be worth something.")
    this.spriteset := 33
    this.unique := false
    this.basevalue := 500
    this.rarity := 5
    this.flags += GENERATE_SINGLE
    this.possess_tag := tag:Engagement Ring
    this.difficulty := 200
 
    this := default_itemdef(itemdef:tiara, $"princess's tiara")
    this.description! := savestring($"A silver tiara, owned by princess Anastasia. It'll probably fetch a pretty penny.")
    this.spriteset := 32
    this.unique := false
    this.basevalue := 1300
    this.rarity := 0
    this.possess_tag := tag:Tiara

    this := default_itemdef(itemdef:royal_talisman, $"Royal Talisman")
    this.description! := savestring($"A magical artifact forged for kings to do battle; whomever possesses it recieves protection from harm.")
    this.unique := false
    this.flags += STACKABLE
    this.spriteset := 45
    this.spritetype := spritetype:walkabout
    this.rarity := 2  # ???

    ########################################### ARMOUR

    this := armour_itemdef(itemdef:tunic, $"tunic")
    this.description! := savestring($"Common garb.")
    this.spriteset := 2
    this.basevalue := 10
    this.itemtype := ITEM_CLOTH
    this.coverage := 40
    this.defense := 1
    this.rarity := 70
    this.difficulty := 100

    this := armour_itemdef(itemdef:robe, $"robe")
    this.description! := savestring($"A garment worn by both wizards and wanderers.")
    this.spriteset := 19
    this.basevalue := 20
    this.itemtype := ITEM_CLOTH
    this.coverage := 75
    this.defense := 2
    this.rarity := 70
    this.difficulty := 200

    this := armour_itemdef(itemdef:platemail, $"platemail")
    this.description! := savestring($"A metal chestplate, typically worn by warriors.")
    this.spriteset := 17
    this.basevalue := 20
    this.itemtype := ITEM_PLATEARMOUR
    this.coverage := 60
    this.defense := 5
    this.rarity := 25
    this.difficulty := 400

    this := armour_itemdef(itemdef:leather, $"leather")
    this.description! := savestring($"Leather armour, preferred by rogues. It provides some defence, and easy movement.")
    this.spriteset := 18
    this.basevalue := 20
    this.itemtype := ITEM_CLOTH
    this.coverage := 60
    this.defense := 3
    this.rarity := 45
    this.difficulty := 200

    this := armour_itemdef(itemdef:chainmail, $"chainmail")
    this.description! := savestring($"A mail made of interlocking steel rings.")
    this.spriteset := 29
    this.basevalue := 20
    this.itemtype := ITEM_OTHERARMOUR
    this.coverage := 85
    this.defense := 4
    this.rarity := 25
    this.difficulty := 300

    this := armour_itemdef(itemdef:stonemail, $"stonemail")
    this.description! := savestring($"A big, burly chestplate, made of solid granite.")
    this.spriteset := 30
    this.basevalue := 20
    this.itemtype := ITEM_OTHERARMOUR
    this.coverage := 65
    this.defense := 6
    this.rarity := 15
    this.difficulty := 400

    this := armour_itemdef(itemdef:leathersteel, $"leathersteel")
    this.description! := savestring($"A hybrid of both leather and steel armor.")
    this.spriteset := 31
    this.basevalue := 20
    this.itemtype := ITEM_PLATEARMOUR
    this.coverage := 90
    this.defense := 5
    this.rarity := 15
    this.difficulty := 400

    ########################################### WEAPONS

    this := weapon_itemdef(itemdef:sword, $"sword")
    this.description! := savestring($"A simple metal sword, bearing a long, well forged blade. They are a standard weapon for any adventurer who engages in melee combat.")
    this.spriteset := 1
    this.damage_min := 4
    this.damage_max := 7
    this.destructive := 7
    this.itemtype := ITEM_BLADE
    this.rarity := 80
    this.difficulty := 200

    this := weapon_itemdef(itemdef:dagger, $"dagger")
    this.description! := savestring($"A small, swift weapon, which is useful for stabbing attacks. Daggers are favored by rogues, who appreciate the weapon's sleek, swift capabilities.")
    this.spriteset := 5
    this.damage_min := 3
    this.damage_max := 5
    this.coverage := 5
    this.destructive := 3
    this.speed_bonus := 50
    this.itemtype := ITEM_BLADE
    this.rarity := 80
    this.difficulty := 100

    this := weapon_itemdef(itemdef:staff, $"staff")
    this.description! := savestring($"A gnarled, wooden staff, crafted from the branch of an ancient tree. It is a useful instrument for a mage to channel their sorceries, though of no such use to others.")
    this.spriteset := 6
    this.damage_min := 1
    this.damage_max := 4
    this.coverage := 70
    this.defense := 6
    this.flags += CHANNEL_MAGIC
    this.itemtype := ITEM_OTHERWEAPON
    this.destructive := 1
    this.rarity := 60
    this.difficulty := 200

    this := weapon_itemdef(itemdef:lightning_staff, $"staff of lightning")
    this.description! := savestring($"An enchanted gnarled, ancient branch, which strikes foes with powerful magical shocks. Can only be used a limited number of times.")
    this.spriteset := 35
    this.damage_min := 15
    this.damage_max := 25
    this.maxcharges := 12
    this.coverage := 70
    this.defense := 6
    this.flags += CHANNEL_MAGIC + MAGIC_DAMAGE
    this.itemtype := ITEM_OTHERWEAPON
    this.destructive := 1
    this.rarity := 27
    this.difficulty := 300

    this := weapon_itemdef(itemdef:scimitar, $"scimitar")
    this.description! := savestring($"A sturdy weapon, bearing a vicious, curved blade.")
    this.spriteset := 7
    this.damage_min := 6
    this.damage_max := 10
    this.coverage := 50
    this.destructive := 11
    this.itemtype := ITEM_BLADE
    this.rarity := 50
    this.difficulty := 300

    this := weapon_itemdef(itemdef:katana, $"katana")
    this.description! := savestring($"A foreign weapon, known for its graceful, slender blade. It can easily slice through many enemies, and can be used by rogues and warriors alike.")
    this.spriteset := 8
    this.damage_min := 8
    this.damage_max := 11
    this.coverage := 50
    this.destructive := 11
    this.itemtype := ITEM_BLADE
    this.rarity := 30
    this.difficulty := 300

    this := weapon_itemdef(itemdef:scythe, $"scythe")
    this.description! := savestring($"A sweeping weapon, with a long handle and a curved blade. It can cut through enemies as though they were crops. It can also channel magic when used by a mage.")
    this.spriteset := 9
    this.damage_min := 5
    this.damage_max := 13
    this.coverage := 30
    this.destructive := 15
    this.flags += CHANNEL_MAGIC
    this.itemtype := ITEM_BLADE
    this.rarity := 20
    this.difficulty := 330

    this := weapon_itemdef(itemdef:wand, $"wand")
    this.description! := savestring($"An instrument of magic, capable of amplifying sorceries, but only of use to trained mages. Its form is that of a small, metallic rod, with a shimmering orb of colored glass at its tip.")
    this.spriteset := 16
    this.damage_min := 1
    this.damage_max := 1
    this.coverage := 0
    this.defense := 0
    this.flags += CHANNEL_MAGIC + MAGIC_DAMAGE
    this.itemtype := ITEM_OTHERWEAPON
    this.destructive := 1
    this.rarity := 40
    this.difficulty := 200

    this := weapon_itemdef(itemdef:machete, $"machete")
    this.description! := savestring($"A weapon smaller than a sword, but much larger than a dagger. Its blade is broader, and it's suited for slashing and stabbing.")
    this.spriteset := 13
    this.damage_min := 8
    this.damage_max := 12
    this.coverage := 40
    this.destructive := 10
    this.defense := 20
    this.itemtype := ITEM_BLADE
    this.rarity := 30
    this.difficulty := 200

    this := weapon_itemdef(itemdef:handaxe, $"hand axe")
    this.description! := savestring($"A weapon that is much like a woodcutting axe, but its blade is suited for chopping monsters rather than trees.")
    this.spriteset := 10
    this.damage_min := 7
    this.damage_max := 14
    this.coverage := 20
    this.destructive := 10
    this.itemtype := ITEM_OTHERWEAPON
    this.rarity := 30
    this.difficulty := 100

    this := weapon_itemdef(itemdef:battleaxe, $"battle axe")
    this.description! := savestring($"A large, vicious, two sided axe. It's a formidable weapon, and is also a formidable size.")
    this.spriteset := 11
    this.damage_min := 5
    this.damage_max := 20
    this.coverage := 50
    this.destructive := 15
    this.defense := 20
    this.itemtype := ITEM_OTHERWEAPON
    this.rarity := 26
    this.difficulty := 280

    this := weapon_itemdef(itemdef:rapier, $"rapier")
    this.description! := savestring($"A sleek, thin sword, well suited to quick, graceful combat.")
    this.spriteset := 12
    this.damage_min := 9
    this.damage_max := 14
    this.coverage := 55
    this.destructive := 2
    this.defense := 16
    this.speed_bonus := 20
    this.itemtype := ITEM_BLADE
    this.rarity := 30
    this.difficulty := 200

    this := weapon_itemdef(itemdef:claymore, $"claymore")
    this.description! := savestring($"A very long sword, which has excellent sweeping abilities, but needs more strength to control.")
    this.spriteset := 27
    this.damage_min := 9
    this.damage_max := 16
    this.coverage := 70
    this.speed_bonus := -20
    this.destructive := 12
    this.defense := 19
    this.itemtype := ITEM_BLADE
    this.rarity := 15
    this.difficulty := 300

    this := weapon_itemdef(itemdef:greatsword, $"greatsword")
    this.description! := savestring($"It's much larger and thicker than a regular sword, and is more powerful and quite heavy.")
    this.spriteset := 28
    this.damage_min := 10
    this.damage_max := 18
    this.coverage := 70
    this.speed_bonus := -30
    this.destructive := 14
    this.defense := 16
    this.itemtype := ITEM_BLADE
    this.rarity := 10
    this.difficulty := 300

    this := weapon_itemdef(itemdef:metalclaws, $"metal claws")
    this.description! := savestring($"A pair of gloves, which have long, metallic claws protruding from the top of the hands.")
    this.spriteset := 14
    this.damage_min := 1
    this.damage_max := 20
    this.coverage := 70
    this.destructive := 9
    this.defense := 11
    this.itemtype := ITEM_BLADE
    this.rarity := 15
    this.difficulty := 300

    this := weapon_itemdef(itemdef:invidia, $"Invidia")
    this.description! := savestring($"A specially forged katana, bearing bloodstained runes on its blade. It once belonged to a ninja, who murdered his allies out of jealously of their abilities.")
    this.spriteset := 20
    this.unique := true
    this.damage_min := 14
    this.damage_max := 19
    this.coverage := 70
    this.destructive := 50
    this.defense := 42
    this.itemtype := ITEM_BLADE
    this.rarity := 3
    this.flags += GENERATE_SINGLE
    this.difficulty := 400

    this := weapon_itemdef(itemdef:ira, $"Ira")
    this.description! := savestring($"A horrible, bloodstained battle axe, belonging to a berserk war general who would murder all in his path.")
    this.spriteset := 22
    this.unique := true
    this.damage_min := 10
    this.damage_max := 25
    this.coverage := 80
    this.destructive := 55
    this.defense := 50
    this.itemtype := ITEM_OTHERWEAPON
    this.rarity := 2
    this.flags += GENERATE_SINGLE
    this.difficulty := 400

    this := weapon_itemdef(itemdef:superbia, $"Superbia")
    this.description! := savestring($"An enchanted rapier. It was forged for a vain noble, who was exiled as a heretic for trying to become a god.")
    this.spriteset := 23
    this.unique := true
    this.damage_min := 15
    this.damage_max := 17
    this.coverage := 50
    this.destructive := 5
    this.defense := 45
    this.speed_bonus := 20
    this.itemtype := ITEM_BLADE
    this.rarity := 2
    this.flags += GENERATE_SINGLE
    this.difficulty := 400

    this := weapon_itemdef(itemdef:acedia, $"Acedia")
    this.description! := savestring($"A massive greatsword, reknown for strength, but not its swiftness. It belonged to a warrior too sluggish and lazy to ever do battle.")
    this.spriteset := 21
    this.unique := true
    this.damage_min := 15
    this.damage_max := 27
    this.coverage := 80
    this.destructive := 50
    this.defense := 49
    this.speed_bonus := -20
    this.itemtype := ITEM_BLADE
    this.rarity := 3
    this.flags += GENERATE_SINGLE
    this.difficulty := 400

    this := weapon_itemdef(itemdef:avaritia, $"Avaritia")
    this.description! := savestring($"A gorgeous, golden scythe, reinforced with magic. It once belong to a merchant who desired to own all the gold in the world.")
    this.spriteset := 24
    this.unique := true
    this.damage_min := 6
    this.damage_max := 21
    this.coverage := 45
    this.destructive := 35
    this.defense := 40
    this.itemtype := ITEM_BLADE
    this.rarity := 2
    this.flags += GENERATE_SINGLE
    this.difficulty := 400

    this := weapon_itemdef(itemdef:gula, $"Gula")
    this.description! := savestring($"A large, enchanted scimitar. It was said to be owned by a voracious warrior who was said to devour his enemies on the battle field.")
    this.spriteset := 25
    this.sprite_width := 9
    this.unique := true
    this.damage_min := 16
    this.damage_max := 23
    this.coverage := 80
    this.destructive := 45
    this.defense := 42
    this.itemtype := ITEM_BLADE
    this.rarity := 2
    this.flags += GENERATE_SINGLE
    this.difficulty := 400

    this := weapon_itemdef(itemdef:luxuria, $"Luxuria")
    this.description! := savestring($"A pair of black metal claws, forged by a woman to punish her adulterous lover.")
    this.spriteset := 15
    this.unique := true
    this.damage_min := 5
    this.damage_max := 30
    this.coverage := 80
    this.destructive := 40
    this.defense := 35
    this.itemtype := ITEM_BLADE
    this.rarity := 2
    this.flags += GENERATE_SINGLE
    this.difficulty := 400

    this := weapon_itemdef(itemdef:inquies, $"Inquies")
    this.description! := savestring($"Inquies, the staff of dreams, once belonged to the Somniatus, the somewhat deranged, and powerful sorceror. Its graceful form seems to be a mere mirage.")
    this.spriteset := 4
    this.unique := true
    this.damage_min := 1
    this.damage_max := 15
    this.coverage := 80
    this.destructive := 3
    this.defense := 40
    this.itemtype := ITEM_OTHERWEAPON
    this.rarity := 0

    this := weapon_itemdef(itemdef:lumia, $"Lumia")
    this.description! := savestring($"A gorgeous, ivory rapier, belonging to Princess Anastasia. Anastasia had ascended realms, leaving her world behind for a beautiful new one. Her divine rapier reflects who she was.")
    this.basevalue := 1000
    this.spriteset := 3
    this.unique := true
    this.damage_min := 4
    this.damage_max := 13
    this.defense := 40
    this.coverage := 40
    this.speed_bonus := 20
    this.destructive := 3  # hardly damages equipment
    this.itemtype := ITEM_BLADE
    this.rarity := 0

end
