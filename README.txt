     Carcere Vicis - version 0.13 (17-03-2014)
        Copyright (C) 2014 TMC & Master K

A roguelike created for the 2014 Random Collaboration
Contest.

'Complete' in the start-to-end sense, but probably highly
unbalanced and still buggy. Help available in-game.
You don't need to read any of this file.

Requires version Callipygous or later of the OHRRPGCE.
Download for Windows, Mac or GNU/Linux from
 http://www.hamsterrepublic.com/ohrrpgce

NOTE: as of writing, gfx_directx is not supported. If you
are using a build of the OHRRPGCE different to the one
provided here, and you are on Windows, then you will
probably get an error message saying thar gfx_sdl is
required. Simply remove gfx_directx.dll to run the game.
Mac and GNU/Linux users will have no problems.

CHANGELOG:

version 0.13 (2014-03-17; only released 2017-01-05!)
  This version fixes buckets of bugs (there are still a few left),
  made numerous changes to balancing and mechanics (it is still not,
  however, balanced), some graphics and texts, tweaks of all kinds,
  and added some new features:
    
    -more item info in the inventory menu
    -preview weapon/armour info when picking up
    -more sound effects
    -added Shade enemy
    -added greater potion of healing
    -added lightning staves, with fixed charges
    -added magic damage
    -added 'parry' damage text
    -added item outlines
    -save files are versioned now
    -shadow enemies now appear everywhere but Mundane

version 0.10 (2014-03-04)
  Initial (contest) release. Turned out to be
  extremely buggy and unbalanced.


LICENSE AND CODE:

The OHRRPGCE is free software available under the GNU GPL.

You may redistribute original or modified versions of this
game as long as attribution is given and it is clearly marked
as modified. (However, beware that some music is ripped.)
The git repo is at:
  https://bitbucket.org/rbv/carcere-vicis

Also, the full scripts files are provided and released into the
public domain (you may use them in any way without
attribution) in case they are useful, however they are not
normal HamsterSpeak scripts. They require the 'HSPP'
preprocessor to compile, source code for which is available at
  https://bitbucket.org/rbv/ohrrpgce/branch/hspp
However you almost certainly don't want to use it.  Some of
the scripts may be useful without it, particularly the
field-of-view code in fov.hspp, which was ported from Eben
Howard's improved recursive shadowcasting code.  (See that
file for details.)



MUSIC CREDITS (shamelessly used):

CRAGMAW CAVERN: Cave Music, Vikings of Midgard
FERROUS PRISON: Crysis 3 OST- Cave Ambient
MT. TEHMCEE: Ambient Trance - Crystal Cavern, by ferretbassit11
MASUTA FOREST: Fantasy Music - Woodland Nymph, by Derek Fiechter
TOWER OF DREAMS: Relaxing Fantasy Music - Awake in a Dream, by AdrianvonZiegler
BASEMENT OF NIGHTMARES: Kammarheit - Spatium
AURUM PALACE: Long Gone Symphony, by toolatepiano
ABYSSAL RIFT:  Theme of Maiden Astraea, Demons Souls
OTHERWORLDLY CATACOMB: Creepy Cave Music - Gemstone Caves, by Derek Fiechter
THE NEXUS: Deadmau5 - Suite 02 (Yeknowitable Remix)
THE PAINTED LIGHTHOUSE: Scarless Arms - trip down memory lane

VS SOMNIATUS: Mega Boss Battle, Vikings of Midgard, by Artimus Bena
VS ANASTASIA: Nintendo A La Cziffra-Tetris Theme A

VILLAGE: Daydream Fantasy - Relax Music
